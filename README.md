Dynomod - a quick and dangerous way to modify DynamoDB tables. Pull a table locally to a JSON file, make edits in your favorite text editor, and then update the table.

## Pre-requisits

In order for Dynomod to access your table, you'll need to make sure to have you credentials stored in a profile within the `~/.aws/credentials` file. Here's an example:

```
[your-profile-name]
aws_access_key_id=XXXX
aws_secret_access_key=XXXX
```

----

## Install

Install the Dynomod CLI globally:

```
npm -g i git+https://brustj@bitbucket.org/brustj/portfolio-site-2022.git
```

Run `dynomod -v` to ensure that the CLI is installed correctly. If an error occurs when trying to install the CLI, try adding `sudo` to the beginning of the install command above.

----

## Usage

#### Getting started

In order to use any of the main Dynomod commands, you must configure the Dynomod Config in order for the CLI to know what AWS account and table to reference. Run `dynomod setup` to create the config.

You'll need to supply the following:
1. Your credentials profile
2. The region that the table is in (ex. us-east-1)
3. The table name
4. The hash key name (ex. id)

#### Commands

```
dynomod [command]
```

| Command | Description |
| ------- | ----------- |
| `get-items` | Pull the entire table locally. The contents will be dropped in `dist/items.json`. |
| `update-items` | Update the table based on whatever is in the `dist/items.json` file. This task will *add*, *modify*, and *delete* table items depending on the edits made to the local JSON file. |
| `delete-all` | Delete all items within the table (not the table itself). |
| `update` | Install the latest version of the CLI. |
| `version` | Log out the current CLI version. |
| `help` | See a help menu with all the commands noted above. |

----

## Local Dev

Clone the package:
```
git clone https://brustj@bitbucket.org/brustj/portfolio-site-2022.git
```

Navigate to the repo's root folder and run `npm link` to link the Dynomod CLI to your local repo.