const minimist = require('minimist');
const chalk = require('chalk');

module.exports = () => {

  const args = minimist(process.argv.slice(2));

  let cmd = args._[0] || 'help';

  if (args.version || args.v) {
    cmd = 'version';
  }

  if (args.help || args.h) {
    cmd = 'help';
  }

  switch (cmd) {

    case 'setup':
      require('./src/commands/setup')(args);
      break;

    case 'get-items':
      require('./src/commands/get-items')(args);
      break;

    case 'update-items':
      require('./src/commands/update-items')(args);
      break;

    case 'delete-all':
      require('./src/commands/delete-all')(args);
      break;

    case 'update':
      require('./src/commands/update')(args);
      break;

    case 'version':
      require('./src/commands/version')(args);
      break;

    case 'help':
      require('./src/commands/help')(args);
      break;

    default:
      console.log(chalk.redBright('Command "${cmd}" not found'));
      console.log(`Visit ${chalk.bold('https://bitbucket.org/brustj/dynomod-cli/src/main/README.md')} for documentation about this command`)
      break;

  }

};