
const chalk = require('chalk');

const logInvalidConfigError = () => {
  console.log(chalk.redBright(`Invalid config.js file`));
  console.log(`Please use the ${chalk.bold('setup')} command to get started`);
};

module.exports = {
  logInvalidConfigError
};