
require('shelljs/global');
const fs = require('fs');
const inquirer = require('inquirer');
const AWS = require('aws-sdk');
const ora = require('ora');
const chalk = require('chalk');

const { logInvalidConfigError } = require('../utils');

const spinner = ora();

if (!fs.existsSync('~/.dynomod/config.js')) {
  logInvalidConfigError();
}

const { tableName = null, profile = null, region = null, hashKey = null } = require('~/.dynomod/config.js');

if (!tableName || !profile || !region || !hashKey) {
  logInvalidConfigError();
}

AWS.config.credentials = new AWS.SharedIniFileCredentials({ profile });
AWS.config.region = region;

const dynamodb = new AWS.DynamoDB.DocumentClient();

const OUTPUT_PATH = 'dist';

const getItems = async () => {

  let items = [], lastEvaluatedKey = '';
  const params = {
    TableName: tableName
  };

  do {
    const data = await dynamodb.scan(params).promise();
    lastEvaluatedKey = data.LastEvaluatedKey;

    items.push(...data.Items);
    params.ExclusiveStartKey = lastEvaluatedKey
  } while (typeof lastEvaluatedKey !== 'undefined');

  return items;

};

module.exports = async () => {

  const confirm = await inquirer.prompt({
    type: 'confirm',
    name: 'shouldGetItems',
    message: `Are you sure you want to get all items from ${tableName} (${region})?`
  });

  if (!confirm.shouldGetItems) {
    console.log(chalk.redBright('Get items aborted'));
    process.exit(0);
  }

  spinner.start();

  try {

    const items = await getItems();

    spinner.stop();

    if (!fs.existsSync(OUTPUT_PATH)) {
      fs.mkdirSync(OUTPUT_PATH, { recursive: true });
    }

    fs.writeFileSync(`${OUTPUT_PATH}/items.json`, JSON.stringify(items, null, 2));

    console.log(`Items can be found at ${chalk.bold(`${OUTPUT_PATH}/items.json`)}`);
    console.log('Get items complete');

  } catch (err) {
    spinner.stop();
    throw err;
  }

};
