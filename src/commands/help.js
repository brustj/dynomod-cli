module.exports = (args) => {
  
  console.log(`
  Usage: dynomod [command] <options>

  Options:

    -v, --version ....... output the version number
    -h, --help .......... output usage information

  Commands:

    - setup ............. create/modify dynomod config file with DynamoDB table details

    - get-items ......... get all items from table and place in dist/items.json
    - update-items ...... update table based on data in dist/items.json
    - delete-all ........ delete all items from table (dangerous!)

    - update
    - version
    - help
  `);

};