const fs = require('fs');
const awsProfiles = require('get-aws-profiles');
const inquirer = require('inquirer');
const AWS = require('aws-sdk');
const chalk = require('chalk');

module.exports = async () => {

  const profiles = awsProfiles.get();
  profileNames = Object.keys(profiles);

  const awsData = await inquirer.prompt([
    {
      type: 'list',
      name: 'profile',
      message: 'Select the associated aws profile:',
      choices: profileNames,
    },
    {
      type: 'input',
      name: 'region',
      message: 'Enter the aws region:',
      default: 'us-east-1',
    },
  ]);

  AWS.config.credentials = new AWS.SharedIniFileCredentials({ profile: awsData.profile });
  AWS.config.region = awsData.region;

  const dynamodb = new AWS.DynamoDB;

  const { TableNames: tableNames } = await dynamodb.listTables().promise();

  const tableData = await inquirer.prompt([
    {
      type: 'list',
      name: 'tableName',
      message: 'Select the table that you wish to modify:',
      choices: tableNames
    },
    {
      type: 'input',
      name: 'hashKey',
      message: `Enter the table's hashkey:`,
      default: 'id',
    },
  ]);

  const setup = {
    ...awsData,
    ...tableData
  };

  console.log(JSON.stringify(setup, null, 2));

  const confirm = await inquirer.prompt({
    type: 'confirm',
    name: 'shouldSetup',
    message: 'Is this correct?',
  });

  if (!confirm.shouldSetup) {
    console.log(chalk.redBright('Dynomod setup aborted'));
    process.exit(0);
  }

  fs.writeFileSync(
    '~/.dynomod/config.js',
    `module.exports = ${JSON.stringify(setup, null, 2)};`
  );

  console.log('Dynomod config setup complete. The config can be found at ~/.dynomod/config.js');

};
