const { execSync } = require('child_process');
const ora = require('ora');

const { repository, version: prevVersion } = require('../../package.json');

const spinner = ora();

module.exports = (args) => {

  console.log('Checking for updates...');
  spinner.start();

  const nextVersion = execSync(`npm view git+${repository} version`, { encoding: 'utf8' }).trim();

  spinner.stop();

  if (nextVersion === prevVersion) {
    console.log(`Dynomod is already up to date (v${prevVersion})`);
  } else {
    execSync(`npm -g i git+${repository}`);
    console.log(`Dynomod update complete (v${prevVersion} -> v${nextVersion})`)
  }
  
};