
require('shelljs/global');
const fs = require('fs');
const inquirer = require('inquirer');
const AWS = require('aws-sdk');
const ora = require('ora');
const chalk = require('chalk');

const { logInvalidConfigError } = require('../utils');

const spinner = ora();

if (!fs.existsSync('~/.dynomod/config.js')) {
  logInvalidConfigError();
}

const { tableName = null, profile = null, region = null, hashKey = null } = require('~/.dynomod/config.js');

if (!tableName || !profile || !region || !hashKey) {
  logInvalidConfigError();
}

AWS.config.credentials = new AWS.SharedIniFileCredentials({ profile });
AWS.config.region = region;

const dynamodb = new AWS.DynamoDB.DocumentClient();

const deleteItems = async () => {

  let items = [], lastEvaluatedKey = '';
  const params = {
    TableName: tableName
  };

  do {
    const data = await dynamodb.scan(params).promise();
    lastEvaluatedKey = data.LastEvaluatedKey;

    items.push(...data.Items);
    params.ExclusiveStartKey = lastEvaluatedKey
  } while (typeof lastEvaluatedKey !== 'undefined');

  const deletePromises = items.map((item) =>
		dynamodb.delete({
			TableName: tableName,
			Key: {
        [hashKey]: item[hashKey]
      }
		})
		.promise()
	);

  return Promise.all(deletePromises);

};

module.exports = async () => {

  console.log(chalk.yellowBright('Warning: This action cannot be undone and is considered dangerous!'));

  const confirm = await inquirer.prompt({
    type: 'input',
    name: 'shouldDelete',
    message: `Please type "delete ${tableName}" to continue:`
  });

  if (confirm.shouldDelete !== `delete ${tableName}`) {
    console.log(chalk.redBright('Delete items aborted'));
    process.exit(0);
  }

  spinner.start();

  try {

    await deleteItems();

    spinner.stop();
  
    console.log('Delete items complete');

  } catch (err) {
    spinner.stop();
    throw(err);
  }

};