
require('shelljs/global');
const fs = require('fs');
const inquirer = require('inquirer');
const chalk = require('chalk');
const AWS = require('aws-sdk');
const diff = require('diff-arrays-of-objects');
const ora = require('ora');

const { logInvalidConfigError } = require('../utils');

const spinner = ora();

if (!fs.existsSync('~/.dynomod/config.js')) {
  logInvalidConfigError();
}

const { tableName = null, profile = null, region = null, hashKey = null } = require('~/.dynomod/config.js');

if (!tableName || !profile || !region || !hashKey) {
  logInvalidConfigError();
}

AWS.config.credentials = new AWS.SharedIniFileCredentials({ profile });
AWS.config.region = region;

const dynamodb = new AWS.DynamoDB.DocumentClient();

const nextItems = require('../../dist/items.json');

const getItems = async () => {

  let items = [], lastEvaluatedKey = '';
  const params = {
    TableName: tableName
  };

  do {
    const data = await dynamodb.scan(params).promise();
    lastEvaluatedKey = data.LastEvaluatedKey;

    items.push(...data.Items);
    params.ExclusiveStartKey = lastEvaluatedKey
  } while (typeof lastEvaluatedKey !== 'undefined');

  return items;

};

const updateItems = async (items) => 
  Promise.all(items.map((item) =>
    dynamodb.put({
      TableName: tableName,
      Item: {...item}
    })
    .promise()
  ));

const deleteItems = async (hashKeys) =>
  Promise.all(hashKeys.map((hash) =>
		dynamodb.delete({
			TableName: tableName,
			Key: {
        [hashKey]: hash
      }
		})
		.promise()
	));

module.exports = async () => {

  if (!nextItems || !nextItems.length) {
    console.log(chalk.redBright('No items found - dynomod update-items aborted'));
  }

  console.log('Comparing updated items against existing table...');

  spinner.start();

  try {

    const prevItems = await getItems();

    spinner.stop();

    const hashKeyDiff  = diff(prevItems, nextItems),
          addedItems   = hashKeyDiff.added.map((item) => item[hashKey]),
          updatedItems = hashKeyDiff.updated.map((item) => item[hashKey]),
          deletedItems = hashKeyDiff.removed.map((item) => item[hashKey]);

    if (!addedItems.length && !updatedItems.length && !deletedItems.length) {
      console.log(chalk.redBright('No updates found - dynomod update-items aborted'));
      process.exit(0);
    }

    addedItems.forEach((hash) => console.log(`+ Added item ${hashKey} = ${hash}`));
    updatedItems.forEach((hash) => console.log(chalk.yellowBright(`~ Modified item ${hashKey} = ${hash}`)));
    deletedItems.forEach((hash) => console.log(chalk.redBright(`- Removed item ${hashKey} = ${hash}`)));

    const confirm = await inquirer.prompt({
      type: 'confirm',
      name: 'shouldUpdate',
      message: `Are you sure you want to update "${tableName}"?`
    });

    if (!confirm.shouldUpdate) {
      console.log(chalk.redBright('Update items aborted'));
      process.exit(0);
    }

    spinner.start();

    if (!!deletedItems.length) {
      await deleteItems(deletedItems);
    }

    await updateItems(nextItems);

    spinner.stop();
  
    console.log('Update items complete');

  } catch (err) {
    spinner.stop();
    throw(err);
  }

};